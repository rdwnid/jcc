Soal 1 Membuat Database
create database myshop;

Soal 2 Membuat Table di Dalam Database

//Menggunakan database myshop
use myshop;

// Membuat tabel users
create table users (id int(8) primary key auto_increment, name varchar(255), email varchar(255), password varchar(255));

// Membuat tabel categories 
create table categories (id int(8) primary key auto_increment, name varchar(255));

// Membuat tabel items
create table items (id int(8) primary key auto_increment, name varchar(255), description varchar(255), price integer, stock integer, category_id integer, foreign key (category_id) references categories(id));

Soal 3 Memasukkan Data pada Table
// Masukan data ke tabel users
insert into users (name, email, password) values ('John Doe', 'john@doe.com', 'john123');
insert into users (name, email, password) values ('Jane Doe', 'jane@doe.com', 'jenita123');

// Masukan data ke tabel categories
insert into categories (name) VALUES ('gadget');
insert into categories (name) VALUES ('cloth');
insert into categories (name) VALUES ('men');
insert into categories (name) VALUES ('women');
insert into categories (name) VALUES ('branded');

// Masukan data ke tabel items
insert into items (name, description, price, stock, category_id) values ('Sumsang b50', 'hp keren dari merk sumsang', 4000000, 100, 1);
insert intoitems (name, description, price, stock, category_id) values ('Uniklooh', 'baju keren dari brand ternama', 500000, 50, 2);
insert into items (name, description, price, stock, category_id) values ('IMHO Watch', 'jam tangan anak yang jujur banget', 2000000, 10, 1);

Soal 4 Mengambil Data dari Database
// a. Mengambil data users
select id, name, email from users;

// b. Mengambil data items
// Menampilkan data items harga lebih dari 1.000.000
select * from items where price >= 1000000;

//mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”
select * from items where name like'%uniklo%';

// c. Menampilkan data items join dengan kategori
select `items`.`name`, `items`.`description`, `items`.`price`, `items`.`stock`, `items`.`category_id`, `categories`.`name` from `items` inner join`categories` on `items`.`category_id` = `categories`.`id`;

Soal 5 Mengubah Data dari Database
update `items` set `price` = 2500000 where`name` = 'Sumsang b50';